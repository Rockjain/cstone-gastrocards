import React, { Component } from 'react';
import { StyleSheet, ImageBackground,Image,TouchableOpacity, AsyncStorage,Alert, Text, View } from 'react-native';
import SplashStyles from './style';
import HeaderView from '../../../Config/Header';
import FlatButton from '../../../Config/Button/Button.js';
import StatusBar from '../../../Assets/StatusBar';
export default class SelectionPage extends React.Component {
  static navigationOptions = {
    header: null
  };

reg=()=>{
  this.props.navigation.navigate('RegPage')
}
login=()=>{
  this.props.navigation.navigate('LoginPage')
}
  render() {
    return (
        <View style={{ flex: 1,backgroundColor:'#122033',alignItems:'center', }}>
        <HeaderView/>

        <View style={{marginTop:50,justifyContent:'center',alignItems:'center'}}>
         <Text style={{ color: '#F2F6F8', fontSize: 30 }}>
            Gastro Cards
          </Text>
          </View>
           <View style={{ flexDirection:'column', alignItems:'center',justifyContent:'center' }}>
            <Text style={{ color: '#F2F6F8', fontSize: 15,}}>
              An Application developed by the American
           </Text>
            <Text style={{ color: '#F2F6F8', fontSize: 15 ,marginLeft:24}}>
              Gastroenterological Association as supplement for.
               <Text style={{ color: '#F2F6F8', fontSize: 15,}}>
              DDSEP.
           </Text>
           </Text>
          </View>

          <View style={{flex:1, marginTop:30, width:'99%', justifyContent:'center',alignItems:'center'}}>
            <Text style={{ fontSize: 14 ,justifilyContain: 'center',color:'#F2F6F8' }}>
              Ready to begin?
      </Text>
         <FlatButton title ='LOG IN' onPress={this.login} color ='#FFF' colorText='#122033'/>
          <View style={{ width:'99%', flex:1,marginTop:50, justifilyContain: 'center', flexDirection: 'column',alignItems:'center',}}>
            <Text style={{color:'#F2F6F8',fontSize:14}}>Don't have an AGA account?</Text>
            <Text style={{color:'#F2F6F8',fontSize:14}}>Please click below to create your account</Text>
            <FlatButton title ='Register' onPress={this.reg} color ='#FFF' colorText='#122033'/>
                    
          <View style={{ marginTop:40, justifilyContain: 'center', alignItems:'center' }}>
            <Text style={{color:'#fff',fontSize:14}}>Questions?</Text>
            <Text style={{color:'#fff',fontSize:14}}>Contact AGA at 301-654-2055 or email us at </Text>
            <Text style={{color:'#fff',fontSize:14}}>gastrocards@gastro.org</Text>
          </View> 
 </View>
 </View>
           <StatusBar/>
         </View>
        
    )
  }
}


