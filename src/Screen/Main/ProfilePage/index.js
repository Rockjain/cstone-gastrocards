import React, { Component } from 'react';
import { StyleSheet, Text, View,Image, TextInput,TouchableOpacity, Button, Alert, ActivityIndicator } from 'react-native';
import firebase from '../../../Database/firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from './style';
import AsyncStorage from '@react-native-community/async-storage';
import StatusBar from '../../../Assets/StatusBar';
import HeaderView from '../../../Config/Header';
import FlatButton from '../../../Config/Button/Button.js';


export default class EditProfile extends Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = { 
      displayName: '',
      email: '', 
      password: '',
      FirstName:'',
      LastName:'',
      Contact:'',
      spinner: false
    }
  }

componentWillMount(){
  console.log('gdkjfdkjfjkd')
  AsyncStorage.getItem('Email')
                .then(Email => {
                  AsyncStorage.getItem('Password')
                .then(Pass => {

this.setState({
  email:Email,
  password:Pass,
})

                })
                })
}

AdduserDetails(email,fname,lname,contact){
  if(this.state.email === '' && this.state.Contact === '' && this.state.FirstName === '' && this.state.Last==='') {
       Alert.alert('Enter details to signup!')
     } else {
       this.setState({
         spinner: true,
       })
    firebase.database().ref('UsersList/').push({
        email,
        fname,
        lname,
        contact,
    }).then((data)=>{
        //success callback
        console.log('data ' , data)
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
     }
}

Logout(){
  AsyncStorage.setItem('Email','');
  firebase.auth().signOut();
  this.props.navigation.navigate('LoginPage');
}

  render() {
    return (
       <View style={ {flex: 1,backgroundColor: '#F2F6F8'}}>
        <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{  color: '#fff'}}
                />
    
       <View style={{ marginTop:10,justifyContent: 'center',alignItems: 'center',width:'99%',flexDirection:'row'}}>
<Image  style={{ width:60,height: 60,alignSelf:'center'}}
 source={require('../../../Assets/Images/logo.png')}></Image>
<View style={{flexDirection:'column',marginLeft:10}}>
 <Text style={{ color: '#000', fontSize: 11, }}>
              American
           </Text>
           <Text style={{ color: '#000', fontSize: 11,}}>
             Gastroenterological
           </Text>
           <Text style={{ color: '#000', fontSize: 11, }}>
             Association
           </Text>
</View>
</View>
<View style={{ flex:1, justifyContent:'center',marginLeft:12}}>
                     
                        <Text style={{fontSize:20,color:'#000',fontWeight: '500',marginLeft:10}}>Edit User Profile</Text>
                   
                         <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="First Name"
                            editable={true}
                            onChangeText={(FirstName) => this.setState({ FirstName })}
                            value={this.state.FirstName}
                        />
                         <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Last Name"
                            editable={true}
                            onChangeText={(Last) => this.setState({ Last })}
                            value={this.state.Last}
                        />
                        <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Email Address"
                            editable={false}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                        />

                        <TextInput
                           style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Password"
                            editable={true}
                            onChangeText={(Password) => this.setState({ Password })}
                            value={this.state.Password}
                        />
                  
                    <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
             onPress={() => this.AdduserDetails()}>
        <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      }}>Submit</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
             onPress={() => this.Logout()}>
        <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      }}>Logout</Text>
                        </TouchableOpacity>
                    </View>
       </View>
                       <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
      </View>
    );
  }
}

