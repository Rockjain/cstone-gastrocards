
import React, { Component } from 'react';
import { StyleSheet, Text, View,Image, TextInput,TouchableOpacity, Button, Alert, ActivityIndicator } from 'react-native';
import Textarea from 'react-native-textarea';
import StatusBar from '../../../Assets/StatusBar';
import firebase from '../../../Database/firebase';

import HeaderView from '../../../Config/Header';
import FlatButton from '../../../Config/Button/Button.js';


export default class SuggestNewQuestion extends Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = { 
      Question: '', 
      password: '',
      Ans:'Answer'

    }
  }


 LoginUser = () => {

   this.props.navigation.navigate('Buttomtab');
  }

  AddQuestion=(question,correctoption)=>{
  if(this.state.Question === '' && this.state.Ans === '') {
       Alert.alert('Enter details to !')
     } else {
       this.setState({
         spinner: true,
       })
    firebase.database().ref('quiz/').push({
        question:question,
        correctoption:correctoption,
    }).then((data)=>{
        //success callback
        Alert.alert('Question Added')
        console.log('data ' , data)
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
     }
}

  render() {
    return (
    <View style={ {flex: 1,backgroundColor: '#F2F6F8'}}>
    <HeaderView/>
     {/* <View style={{ marginTop:10,
        justifyContent: 'center',
        alignItems: 'center',width:'99%',flexDirection:'row'}}>
<Image
 style={{ width:60,height: 60,alignSelf:'center'}}
 source={require('../../../Assets/Images/logo.png')}></Image>
<View style={{flexDirection:'column',marginLeft:10}}>
 <Text style={{ color: '#000', fontSize: 11, }}>
              American
           </Text>
           <Text style={{ color: '#000', fontSize: 11,}}>
             Gastroenterological
           </Text>
           <Text style={{ color: '#000', fontSize: 11, }}>
             Association
           </Text>
</View>
</View> */}
<View style={{ flex:1, justifyContent:'center',marginLeft:12,marginRight:10}}>
                     
                        <Text style={{fontSize:20,color:'#000',fontWeight: '500',marginLeft:10}}>Suggest a New Question</Text>
                   
                        {/* <Text style={{ color: 'red', fontSize: 12, marginBottom: 10, fontWeight: '700' }}>* All fields are mandatory</Text>
                        <Text style={{ color: '#3386FF', fontSize: 16, fontWeight: '700' }}>Name</Text> */}
                        <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      
      borderWidth: 1.5 }}
                            placeholder="Question"
                            editable={true}
                            onChangeText={(Question) => this.setState({ Question })}
                            value={this.state.Question}
                        />

                        {/* <TextInput
                           style={{ color: '#000', fontSize: 16, margin: 15,
      height: 140,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Password"
                            editable={true}
                            onChangeText={(Password) => this.setState({ Password })}
                            value={this.state.Password}
                        /> */}
                        <Textarea
    containerStyle={{ height: 180,
    marginRight:10,
    padding: 5,
    backgroundColor: '#F5FCFF',}}
   style={{  textAlignVertical: 'top',  // hack android
    height: 170,
    marginLeft:10,
    marginRight:10,
    borderColor: '#000',
      borderWidth: 1.5 ,
    fontSize: 14,

    color: '#333', }}
    onChangeText={this.onChange}
    //defaultValue={this.state.Ans}
    //maxLength={120}
    placeholder={'Answer'}
    placeholderTextColor={'#c7c7c7'}
    underlineColorAndroid={'transparent'}
  />
                   
                   <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
             //onPress={() => this.AddQuestion()}
             >
        <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      }}>Submit</Text>
                        </TouchableOpacity>
                    </View>
      
                     </View>
                       <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
      </View>
    );
  }
}

