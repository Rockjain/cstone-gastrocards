import React, { Component } from 'react';
import { StyleSheet, Text,ScrollView, View,Image, TextInput,TouchableOpacity, Button, Alert, ActivityIndicator } from 'react-native';
import firebase from '../../../Database/firebase';
import styles from './style';
import Spinner from 'react-native-loading-spinner-overlay';
import StatusBar from '../../../Assets/StatusBar';
import FlatButton from '../../../Config/Button/Button.js';
import HeaderView from '../../../Config/Header';
export default class Signup extends Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = { 
      displayName: '',
      email: '', 
      Password: '',
      First:'',
      Last:'',
      spinner: false,
    }
  }

  // updateInputVal = (val, prop) => {
  //   const state = this.state;
  //   state[prop] = val;
  //   this.setState(state);
  // }

 registerUser(){
    if(this.state.email === '' && this.state.Password === '' && this.state.First === '' && this.state.Last === '') {
      Alert.alert('Enter details to signup!')
    } else {
      this.setState({
        spinner: true,
      })
      firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.Password)
      .then((res) => {

        console.log('ROhit jain'+res.user)
        // res.user.updateProfile({
        //   displayName: this.state.displayName
        // })
        Alert.alert('User registered successfully!')
        console.log('User registered successfully!')
        this.setState({
          spinner: false,
          First: '',
          email: '', 
          Password: '',
          Last:'',
        })
        this.props.navigation.navigate('LoginPage')
      })
      .catch(error =>{
          this.setState({ spinner: false });
      Alert.alert(error.message)
    
      })      
    }
  }

  render() {
    return (
       <View style={ {flex: 1,backgroundColor: '#F2F6F8'}}>
       <ScrollView>
        <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{  color: '#fff'}}
                />
       <View style={{ marginTop:10,justifyContent: 'center',alignItems: 'center',width:'99%',flexDirection:'row'}}>
<Image  style={{ width:60,height: 60,alignSelf:'center'}}
 source={require('../../../Assets/Images/logo.png')}></Image>
<View style={{flexDirection:'column',marginLeft:10}}>
 <Text style={{ color: '#000', fontSize: 11, }}>
              American
           </Text>
           <Text style={{ color: '#000', fontSize: 11,}}>
             Gastroenterological
           </Text>
           <Text style={{ color: '#000', fontSize: 11, }}>
             Association
           </Text>
</View>
</View>
<View style={{flex:1, justifyContent:'center',marginLeft:12}}>


                        <Text style={{fontSize:20,color:'#000',fontWeight: '500',marginLeft:10}}>Register</Text>
                   
                         <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="First Name"
                            editable={true}
                            onChangeText={(First) => this.setState({ First })}
                            value={this.state.First}
                        />
                         <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Last Name"
                            editable={true}
                            onChangeText={(Last) => this.setState({ Last })}
                            value={this.state.Last}
                        />
                        <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Email Address"
                            editable={true}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                        />

                        <TextInput
                           style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Password"
                            editable={true}
                            onChangeText={(Password) => this.setState({ Password })}
                            value={this.state.Password}
                        />
                   
<View style={{alignItems:'center',justifyContent:'center',width:'99%'}}>
                   <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
             onPress={() => this.registerUser()}>
        <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      }}>REGISTER</Text>
                        </TouchableOpacity>
                    </View>
                     <Text style={{ color: '#000', fontSize: 16,marginTop:40, }}>
             if Account already exit?
           </Text>
                      <View style={{ 
      width:'92%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '100%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
                            onPress={() => this.props.navigation.navigate('LoginPage')}>
                            <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      
      }}>Login</Text>
                        </TouchableOpacity>
                    </View>
                    
                     </View>
                     </View>
                     </ScrollView>
                      <StatusBar/>
      </View>
    );
  }
}

