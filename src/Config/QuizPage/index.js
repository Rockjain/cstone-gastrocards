import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
//import Animbutton from './animbutton'
import firebase from '../../Database/firebase';
import Answer  from '../../Config/AnswerQuiz';
import Question from '../../Config/Question';

const { width, height } = Dimensions.get('window')
let arrnew = []
const jsonData =  {
  "quiz": {
    "quiz1": {
      "question1": {
        "correctoption": "Decreased need for endoscopic therapy 2. Decrease in active bleeding",
        "question": "What are the benefits of PPI infusion in acute GI bleeding?"
      },
      "question2": {
        "correctoption": "Low - 33%",
        "question": "What volume of blood from upper GI source is required to pass melena ?"
      },
      "question3": {
        "correctoption": "It does not improve mucosal visualization.",
        "question": "What is the effect of NG lavage on mucosal visualization at time of endoscopy ?"
      },
      "question4": {
        "correctoption": "7 g/dL",
        "question": "What is hemoglobin transfusion threshold in GI bleeding ?"
      },
      "question5": {
        "correctoption": "Incorporates patient factors (age, SBP, mental status) & labs (INR, albumin) to predict in-hospital mortality",
        "question": "What is the AIMS-65 score?"
      },
      "question6": {
        "correctoption": "Low score (0-1) do not typically require hospital admission or urgent endoscopy.",
        "question": "How is Glasgow-Blatchford score used to perform pre-endoscopy risk stratification?"
      },
      "question7": {
        "correctoption": "SBP, hemoglobin, BUN, pulse, melena, syncome, liver disease, heart failure",
        "question": "What are components of Glasgow-Blatchford score ?"
      },
      "question8": {
        "correctoption": "0.05-0.1cc/min",
        "question": "What rate of bleeding is required for tagged RBC scan ?"
      },
      "question9": {
        "correctoption": "60-70%",
        "question": "What is tagged RBC accuracy of predicting bleeding location ?"
      },
      "question10": {
        "correctoption": "0.3-0.5 cc/min",
        "question": "What rate of bleeding is required for CT angiography ?"
      },
      "question11": {
        "correctoption": "0.5-1.0 cc/min",
        "question": "What rate of bleeding is required for angiography ?"
      },
      "question12": {
        "correctoption": "Reduced need for repeat endoscopy (O.R. 0.55)",
        "question": "What is the main benefit of administering prokinetics prior to endoscopy for GI bleeding ?"
      },
      "question13": {
        "correctoption": "Ablative therapy e.g., APC (or RFA)",
        "question": "What is the most effective treatment for GAVE ?"
      },
      "question14": {
        "correctoption": "Systemic sclerosis, cirrhosis",
        "question": "In what conditions is this condition commonly seen ?"
      },
      "question15": {
        "correctoption": "It is typically not effective.",
        "question": "What is the role of non-selective beta-blockers in treating GAVE ?"
      },
      "question16": {
        "correctoption": "Bleeding from esophageal varices",
        "question": "In what bleeding condition does Sengstaken–Blakemore tube provide effective tamponade ?"
      },
      "question17": {
        "correctoption": "1. Peptic ulcer disease 2. Gastroduodenal erosions 3. Esophagitis",
        "question": "What are the top three causes of UGIB (all-comers) ?"
      },
      "question18": {
        "correctoption": "< 3%",
        "question": "Without treatment, what is rebleeing rate of clean based peptic ulcer ?"
      },
      "question19": {
        "correctoption": "50%",
        "question": "Without treatment, what is rebleeing rate of a peptic ulcer with a non-bleeding visible vessel ?"
      },
      "question20": {
        "correctoption": "Up to 35%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with an adherent clot ?"
      },
      "question21": {
        "correctoption": "Up to 27%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with oozing?"
      },
      "question22": {
        "correctoption": "< 3%",
        "question": "Without treatment, what is rebleeing rate of clean based peptic ulcer ?"
      },
      "question23": {
        "correctoption": "< 8%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with a flat pigmented spot?"
      },
      "question24": {
        "correctoption": "Up to 35%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with an adherent clot ?"
      },
      "question25": {
        "correctoption": "Up to 35%",
        "question": "In what % of patients with diverticulosis does lower GI bleeding occur ?"
      },
      "question26": {
        "correctoption": "3-15%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with oozing?"
      },
      "question27": {
        "correctoption": "< 3%",
        "question": "Without treatment, what is rebleeing rate of clean based peptic ulcer ?"
      },
      "question28": {
        "correctoption": "Age >60, Hgb <10 g/dL, bleeding for >6 months, male, overt (rather than obscure) bleeding, proximity to overt bleeding",
        "question": "What are factors that increase diagnostic yield of VCE for GI bleeding ?"
      },
      "question29": {
        "correctoption": "Acute elevation of gastric pH optimizes platelet aggregation & clot formation",
        "question": "What is mechanism of PPI benefit in GI bleeding ?"
      },
      "question30": {
        "correctoption": "If refractory despite iron, octreotide or thalidomide (more side-effects)",
        "question": "What are medical treatment options for small bowel AVMs ?"
      },
      "question31": {
        "correctoption": "< 3%",
        "question": "Without treatment, what is rebleeing rate of clean based peptic ulcer ?"
      },
      "question32": {
        "correctoption": "Posterior duodenal wall & posterior lesser curvature of stomach",
        "question": "What two location of peptic ulcers confer the highest risk for rebleeding ?"
      },
      "question33": {
        "correctoption": "7-10 days",
        "question": "After how many days of holding aspirin does risk of thrombogenic complications increase ?"
      },
      "question34": {
        "correctoption": "If refractory despite iron, octreotide or thalidomide (more side-effects)",
        "question": "What are medical treatment options for small bowel AVMs ?"
      },
      "question35": {
        "correctoption": "Cameron Lesion - surgical repair of hiatus hernia",
        "question": "What is the definitive treatment for this lesion ?"
      },
      "question36": {
        "correctoption": "When taken with NSAIDs, risk of complications is 2X NSAIDS alone",
        "question": "When are corticosteroids associated with increase of ulcer bleeding ?"
      },
      "question37": {
        "correctoption": "Within first month of use",
        "question": "When are NSAIDs most likely to cause bleeding complications ?"
      },
      "question38": {
        "correctoption": "75-80%",
        "question": "In what % of patients will ulcer bleeding spontaneously resolve ?"
      },
      "question39": {
        "correctoption": "Age, prior PUB, medications",
        "question": "What are most important risk factors for peptic ulcer bleeding ?"
      },
      "question40": {
        "correctoption": "Low energy over long duration",
        "question": "What settings should be used if multipolar coagulation is used to treat high risk ulcer stigmata ?"
      },
      "question41": {
        "correctoption": "Within first month of use",
        "question": "When are NSAIDs most likely to cause bleeding complications ?"
      },
      "question42": {
        "correctoption": "Dual > Injection monotherapy (Mechanical &/or thermal monotherapy also > Injection monotherapy)",
        "question": "What is efficacy of injection monotherapy compared to dual therapy ?"
      },
      "question43": {
        "correctoption": "Risk of fistulization",
        "question": "What is the danger of biopsying radiation proctitis ?"
      },
      "question44": {
        "correctoption": "Dieulafoy's lesion",
        "question": "What is this lesion ?"
      },
      "question45": {
        "correctoption": "A dilated aberrant submucosal artery that erodes the overlying epithelium without associated ulcer",
        "question": "What is a Dieulafoy lesion ?"
      },
      "question46": {
        "correctoption": "The proximal stomach",
        "question": "Where do Dieulafoy lesions most commonly occur ?"
      },
      "question47": {
        "correctoption": "Male, cardiovascular disease, hemodialysis, NSAID use",
        "question": "What are risk factors for Dieulafoy lesion ?"
      },
      "question48": {
        "correctoption": "Abdominal CT angiography",
        "question": "What is the most effective modality to assess for an aortoenteric fistula ?"
      },
      "question49": {
        "correctoption": "Decrease in mortality",
        "question": "What is the benefit of antibiotics in cirrhotic patients admitted with GI bleeding ?"
      },
      "question50": {
        "correctoption": "NSAID cessation and PPI therapy for 8 weeks",
        "question": "What is the management of small, NSAID-induced gastric ulcer ?"
      },
      "question51": {
        "correctoption": "Active bleeding, non-bleeding visible vessel",
        "question": "Which ulcer stigmata warrant endoscopic therapy ?"
      },
      "question52": {
        "correctoption": "2.5",
        "question": "Above what INR should reversal agents be considered prior to endoscopy in the setting of GI bleeding ?"
      },
      "question53": {
        "correctoption": "APC (non-contact preferred to contact therapy)",
        "question": "What is the preferred endoscopic treatment for this cause of hematochezia?"
      },
      "question54": {
        "correctoption": "Splenic flexure, rectosigmoid colon (SMA & IMA watershed areas)",
        "question": "What are two most common areas of colon affected by ischemic colitis ?"
      },
      "question55": {
        "correctoption": "CT enterography",
        "question": "What is the recommended next test after negative VCE for suspected small bowel blood loss ?"
      },
      "question56": {
        "correctoption": "Initial low-grade, self limited GI bleed which is often followed by major hemorrhage in aortoenteric fistula",
        "question": "What is a herald bleed ?"
      },
      "question57": {
        "correctoption": "Diverticulosis",
        "question": "What is the most common cause of acute severe hematochezia ?"
      },
      "question58": {
        "correctoption": "5-7 days",
        "question": "What is usual timing of post-polypectomy bleeding ?"
      },
      "question59": {
        "correctoption": "Wash vigorously to try and dislodge clot, followed by attempt to remove with forceps or cold snare. Then assess for SRH.",
        "question": "What is the recommended approach to ulcer with adeherent clot?"
      },
      "question60": {
        "correctoption": "90-150cm",
        "question": "How far does push enteroscopy reach ?"
      },
      "question61": {
        "correctoption": "240-360cm distal to pylorus",
        "question": "How far does antegrade double balloon enteroscopy reach ?"
      },
      "question62": {
        "correctoption": "Gastric varices",
        "question": "Name the finding in this retroflexed gastric image (common cause of severe UGI hemorrhage)?"
      }
    }
  }
}				 			 			 									

//console.log('firebase responce check'+firebase.database.ref());
    const jsonData12=  firebase.database().ref('/').on('value', function (snapshot) {
       //jsonData= snapshot.val();
          console.log(snapshot.val())
         const jsonData1=(snapshot.val())
         //jsonData12=JSON.stringify(jsonData1)
        
          return jsonData1;
    });
    console.log('kapil jain'+jsonData12)
export default class Quiz extends Component {
  constructor(props){
    super(props);
    this.qno = 0
    this.score = 0

    const jdata = jsonData.quiz.quiz1
    arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
    this.state = {
      question : arrnew[this.qno].question,
      options : arrnew[this.qno].options,
      correctoption : arrnew[this.qno].correctoption,
      countCheck : 0,
      initcount:1,
      totallengh:arrnew.length,
      Right:1,
      Worng:1,
      OpenView:'false',
    }
  }
  // prev(){
  //   if(this.qno > 0){
  //     this.qno--
  //     this.setState({ question: arrnew[this.qno].question, options: arrnew[this.qno].options, correctoption : arrnew[this.qno].correctoption})
  //   }
  // }
  Nextview(){
    console.log('kapilklcdcdc'+this.state.OpenView)
    this.setState({
      OpenView:true
    })
  }
  next(){
    this.setState({
      OpenView:false
    })
    if(itemname == 'Currect'){
           this.score += 1
       }else{
      }
    
    if(this.qno < arrnew.length-1){
      this.qno+ 1
console.log('kapil'+this.qno++);



      this.setState({ countCheck: 0,
      initcount:this.state.initcount + 1,
       question: arrnew[this.qno].question, options: arrnew[this.qno].options, correctoption : arrnew[this.qno].correctoption})
    }else{
      
      this.props.quizFinish(this.score)
     }
  }

   _quizFinish(score){    
    this.setState({ visible: true, score : score })
  }
  // _answer(status,ans){

  //   if(status == true){
  //       const count = this.state.countCheck + 1
  //       this.setState({ countCheck: count })
  //       if(ans == this.state.correctoption ){
  //         this.score += 1
  //       }
  //     }else{
  //       const count = this.state.countCheck - 1
  //       this.setState({ countCheck: count })
  //       if(this.state.countCheck < 1 || ans == this.state.correctoption){
  //       this.score -= 1
  //      }
  //     }

  // }
MainRender(){
console.log('kapilkl'+this.state.OpenView)
if(this.state.OpenView =='false'){
  return(
    <View style={{flex:1,alignItems:'center'}}>
 <View style={{ flexDirection: 'column', justifyContent: "space-between", alignItems: 'center',}}>
  <View style={{width:'99%',alignItems:'center',marginTop:20}}>
  <Text style={{color:'#000',fontSize:26,padding:10,fontWeight:'700'}}>Question {this.state.initcount} of {this.state.totallengh} </Text>
  </View>
  <View style={styles.oval} >
        <Text style={styles.welcome}>
          {this.state.question}
        </Text>

     </View>
        <View style={[styles.oval,{marginTop:20, justifyContent:'center',flex:1}]} >
          {/* <Text style={styles.welcome1}>
          {this.state.correctoption}
        </Text> */}
        <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
              onPress={() => this.Nextview()}>
         <Text style={{ color: '#fff',
         fontSize: 14,
        alignSelf: 'center',
     }}>REVEAL ANSWER</Text>
                        </TouchableOpacity>
                  </View>
                     </View> 
      </View> 
      </View>
  )
}else{
  return(
<View style={{flex:1,alignItems:'center'}}>
 <View style={{ flexDirection: 'column', justifyContent: "space-between", alignItems: 'center',}}>
  <View style={{width:'99%',alignItems:'center',marginTop:20}}>
  <Text style={{color:'#000',fontSize:26,padding:10,fontWeight:'700'}}>Question {this.state.initcount} of {this.state.totallengh} </Text>
  </View>
  <View style={styles.oval} >
        <Text style={styles.welcome}>
          {this.state.question}
        </Text>

     </View>
        <View style={[styles.oval,{marginTop:20, justifyContent:'center',flex:1}]} >
          {/* <Text style={styles.welcome1}>
          {this.state.correctoption}
        </Text> */}
        <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
              onPress={() => this.Nextview()}>
         <Text style={{ color: '#fff',
         fontSize: 14,
        alignSelf: 'center',
     }}>REVEAL ANSWER</Text>
                        </TouchableOpacity>
                  </View>
                     </View> 
      </View> 
      </View>
  )
  
}
}
  render() {
    // let _this = this
    // const currentOptions = this.state.options
    // const options = Object.keys(currentOptions).map( function(k) {
    //   return (  <View key={k} style={{margin:10,backgroundColor:'#000'}}>

    //     <Text style={{color:'#000'}}text={currentOptions[k]}></Text>

    //   </View>)
    // });

    return (
    
      <View style={{flex:1}}>
       {/* <ScrollView style={{flex:1, backgroundColor: '#F5FCFF',paddingTop: 10}}> */}
     
      {this.MainRender()}
      
         {/* </ScrollView> */}
      </View>
       
    );
  }
}

const styles = StyleSheet.create({

  oval: {
    padding:10,
  width: width * 94/100,
  borderRadius: 20,
  alignItems:'center',
  justifyContent:'center',
  //flex:.5,
 // backgroundColor: 'green'
  },
  container: {
    flex: 1,
    alignItems: 'center',
     backgroundColor: '#F5FCFF',paddingTop: 10
  },
  welcome: {
    fontSize: 16,
    margin: 4,
    color: "#000",alignItems:'center',
  },
   welcome1: {
    fontSize: 16,
    margin: 4,
    padding:10,
    color: "#000",alignItems:'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});