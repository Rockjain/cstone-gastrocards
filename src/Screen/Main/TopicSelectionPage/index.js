
import React, { Component } from 'react';
import { StyleSheet, Text, View,Image, TextInput,TouchableOpacity, Button, Alert, ActivityIndicator } from 'react-native';
//import firebase from '../Database/firebase';
//import LoginButton from '../../../Config/Button';
//import ProfileStyle from './style';
//import CatTab from '../../../Config/'
import StoreHeader from '../../../Config/Header'
import Textarea from 'react-native-textarea';
import StatusBar from '../../../Assets/StatusBar';
import Commanview from '../../../Component/ReactView';

export default class TopicScreen extends Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();

  }


 LoginUser = () => {

   this.props.navigation.navigate('Buttomtab');
  }
   Quizpanel(item) {
console.log('kbf'+item);

        this.props.navigation.navigate('PlayQuizPage',{ CatNAME:item
             })
    }


  render() {
    return (
    <View style={ {flex: 1,backgroundColor: '#F2F6F8'}}>
     <StoreHeader/>
      <View style={{ flex:1,alignItems:'center', marginLeft:12,marginRight:12}}>
     <Text style={{fontSize:18,color:'#000',fontWeight: '700',marginTop:10}}>Please Select a topic</Text>
    <View style={{marginTop:10,  flexDirection:'row',justifyContent:'space-between',width:'98%',alignItems:'center'}}>
    <Commanview title='BILIARY TRACT' onPress={this.Quizpanel('BILIARY TRACT')}/>
    <Commanview title='COLON' onPress={this.Quizpanel('COLON')}/>

                        {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>  
       <TouchableOpacity     
             onPress={() =>this.Quizpanel('BILIARY TRACT')
             }
             >
             <View style={{justifyContent:'center'}}>
               <Image style={{ width: 42, height: 42,marginLeft:30 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Biliary_Tract.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>BILIARY TRACT</Text>
                         </View>
                       </TouchableOpacity>
                        </View>   */}
                         {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('COLON')}
             >
               <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Colon.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>COLON</Text>
                         </TouchableOpacity>
                        </View> */}

</View>
 <View style={{marginTop:10,  flexDirection:'row',justifyContent:'space-between',width:'98%',alignItems:'center'}}>
    <Commanview title='ESOPHAGUS' onPress={this.Quizpanel('ESOPHAGUS')}/>
    <Commanview title='LIVER' onPress={this.Quizpanel('LIVER')}/>

                        
                        {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('ESOPHAGUS')}
             >
               <Image style={{ alignItems: 'center', width: 42, height: 42,marginLeft:20 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Esophagus.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700',}}>ESOPHAGUS</Text>
                         </TouchableOpacity>
                        </View>   */}
                         {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('LIVER')}
            
          >
            <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Liver.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>LIVER</Text>
                         </TouchableOpacity>
                        </View> */}

</View>
 <View style={{marginTop:10,  flexDirection:'row',justifyContent:'space-between',width:'98%',alignItems:'center'}}>
  
   <Commanview title='PANCREAS' onPress={this.Quizpanel('PANCREAS')}/>
    <Commanview title='SMALL INTESTINE' onPress={this.Quizpanel('SMALL INTESTINE')}/>

  {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('PANCREAS')}
            
          >
        <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Pancreas.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>PANCREAS</Text>
                         </TouchableOpacity>
                        </View>   */}
                         <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('SMALL INTESTINE')}
            
          >
        <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Small_Intestine.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>SMALL INTESTINE</Text>
                       </TouchableOpacity>
                        </View>

</View>
 <View style={{marginTop:10,  flexDirection:'row',justifyContent:'space-between',width:'98%',alignItems:'center'}}>
  
    <Commanview title='STOMACH AND DVODENUM' onPress={this.Quizpanel('STOMACH AND DVODENUM')}/>
    <Commanview title='RANDOM' onPress={this.Quizpanel('RANDOM')}/>
  
  {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
    
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('STOMACH AND DVODENUM')}
            
          >
        <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Stomach_and_Duodenum.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700',marginLeft:30 }} >STOMACH AND DVODENUM</Text>
                        </TouchableOpacity>
                        </View>   */}
                        {/* <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
       <TouchableOpacity
            onPress={() =>this.Quizpanel('RANDOM')}
            
          >
        <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Random.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>RANDOM</Text>
                         </TouchableOpacity>
                        </View> */}
</View>
<View style={{marginTop:10,width:'99%',justifyContent:'center',flex:1,alignItems:'center'}}>
  <View style={{ height: 86,
      width:'51%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.8}}>
        <Image style={{ alignItems: 'center', width: 42, height: 42 }} resizeMode={'stretch'} source={require('../../../Assets/Images/Incorrect_Questions.png')} />
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>INCORRECT QUESTIONS</Text>
                        </View>  
                        </View>
                         </View>
                       <StatusBar/>
                      </View>
    );
  }
}

