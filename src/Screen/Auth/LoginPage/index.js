import React, { Component } from 'react';
import { StyleSheet, Text,ScrollView, View,Image, TextInput,TouchableOpacity, Button, Alert, ActivityIndicator } from 'react-native';
import firebase from '../../../Database/firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import StatusBar from '../../../Assets/StatusBar';
import FlatButton from '../../../Config/Button/Button.js';
import HeaderView from '../../../Config/Header';
export default class LoginPage extends Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = { 
      email: '', 
      password: '',
      spinner:false,

    }
  }


 LoginUser = () => {
 if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signin!')
    } else {
      this.setState({
        spinner: true,
      })
      firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log('Kapil'+res.user.email)
        console.log('User logged-in successfully!')
        AsyncStorage.setItem('Email', res.user.email);
        AsyncStorage.setItem('Password', res.user.password);
        this.setState({
          spinner: false,
          email: '', 
          password: ''
        })
        this.props.navigation.navigate('Buttomtab');
      })
      .catch(error => {
         this.setState({ spinner: false });
      Alert.alert(error.message)
     
      })
    }
  }

  render() {
    return (

                    <View style={ {flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
       // alignItems: 'center',
       // justifyContent: 'center',
     backgroundColor: '#F2F6F8'}}>
     <Spinner
                    visible={this.state.spinner}
                    textContent={'Loading...'}
                    textStyle={{  color: '#fff'}}
                />
    
     <View style={{ marginTop:10,
        justifyContent: 'center',
        alignItems: 'center',width:'99%',flexDirection:'row'}}>
<Image
 style={{ width:60,height: 60,alignSelf:'center'}}
 source={require('../../../Assets/Images/logo.png')}></Image>
<View style={{flexDirection:'column',marginLeft:10}}>
 <Text style={{ color: '#000', fontSize: 11, }}>
              American
           </Text>
           <Text style={{ color: '#000', fontSize: 11,}}>
             Gastroenterological
           </Text>
           <Text style={{ color: '#000', fontSize: 11, }}>
             Association
           </Text>
</View>
</View>
<View style={{ flex:1, justifyContent:'center',marginLeft:12}}>
                     
                        <Text style={{fontSize:20,color:'#000',fontWeight: '500',marginLeft:10}}>Log In</Text>
                   
                        {/* <Text style={{ color: 'red', fontSize: 12, marginBottom: 10, fontWeight: '700' }}>* All fields are mandatory</Text>
                        <Text style={{ color: '#3386FF', fontSize: 16, fontWeight: '700' }}>Name</Text> */}
                        <TextInput
                            style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Email Address"
                            editable={true}
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                        />

                        <TextInput
                           style={{ color: '#000', fontSize: 16, margin: 15,
      height: 40,
      borderColor: '#000',
      borderWidth: 1.5 }}
                            placeholder="Password"
                            editable={true}
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                        />
                   
<View style={{alignItems:'center',justifyContent:'center',width:'99%'}}>
                   <View style={{ 
      width:'92%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
             onPress={() => this.LoginUser()}>
        <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      }}>Log In</Text>
                        </TouchableOpacity>
                    </View>
                     <Text style={{ color: '#000', fontSize: 16,marginTop:40, }}>
             Don't have an AGA account?
           </Text>
                      <View style={{ 
      width:'92%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '92%',
       height: 42,
        backgroundColor: '#122033',
       justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
                            onPress={() => this.props.navigation.navigate('RegPage')}>
                            <Text style={{ color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
      
      }}>REGISTER</Text>
                        </TouchableOpacity>
                    </View>
                    
                     </View>
                     </View>
                    
                       <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
      </View>
      
    );
  }
}

