import React from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import SplashPage from '../Screen/Auth/SplashPage';
import LoginPage from '../Screen/Auth/LoginPage';
import RegPage from '../Screen/Auth/RegPage';
import SelectionPage from '../Screen/Auth/SelectionPage';
import EditProfile from '../Screen/Main/ProfilePage';
import SuggestNewQusPage from '../Screen/Main/SuggestNewQuestionPage';
import TopicSelectionPage from '../Screen/Main/TopicSelectionPage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PlayQuizPage from '../Screen/Main/PlayQuizPage';

const AuthStart = createStackNavigator({
  SelectionPage: {
    screen: SelectionPage
  },
   LoginPage:{
     screen:LoginPage
   },
   RegPage:{
     screen:RegPage
   },
});
const PlayQuizStack = createStackNavigator({
  PlayQuizPage:{
    screen:PlayQuizPage
  }
  
})

const TopicSelectionStack = createStackNavigator({
TopicSelectionPage:{
  screen:TopicSelectionPage
},
});

const EditProfileStack = createStackNavigator({
  EditProfile:{
    screen:EditProfile
  },
});
const SuggestNewQuStack = createStackNavigator({
  SuggestNewQusPage:{
    screen:SuggestNewQusPage
  },
});
const Buttomtab = createBottomTabNavigator(
  {
    Topic: { screen: TopicSelectionStack },
    Suggest:{screen:SuggestNewQuStack},
    Edit:{screen:EditProfileStack},

  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Topic') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Suggest') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        }else if(routeName === 'Edit'){
           iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        }
 return <IconComponent name={iconName} size={22} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#42f44b',
      inactiveTintColor: 'gray',
    },
  }
);
const AuthStack = createSwitchNavigator(
  {
    AuthLoading: SplashPage,
    AuthStart:AuthStart,
    Buttomtab:Buttomtab,
    PlayQuizStack:PlayQuizStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

const RootApp = createAppContainer(AuthStack);
export default RootApp;