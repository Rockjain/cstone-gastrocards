import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import Colors from '../../../Config/Color'; 

const SplashStyles = StyleSheet.create({
  SplashMain: {
    flex: 1,
   justifyContent: 'center',
   alignItems: 'center',
   backgroundColor:Colors.SBColor,

  },
  
})
export default SplashStyles;