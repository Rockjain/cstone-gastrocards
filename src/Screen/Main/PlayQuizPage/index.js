import React, { Component } from 'react';
//import Quiz  from '../../../Config/QuizPage';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  Dimensions,
} from 'react-native';
import HeaderView from '../../../Config/Header'
import StatusBar from '../../../Assets/StatusBar';
import Icon from 'react-native-vector-icons/FontAwesome';
import FlatButton from '../../../Config/Button/Button.js';
const { width, height } = Dimensions.get('window')
let arrnew = []
//let correctQus=[]
let IncorrectQus = []
const jsonData =  {
  "quiz": {
    "quiz1": {
      "question1": {
        "correctoption": "Decreased need for endoscopic therapy 2. Decrease in active bleeding",
        "question": "What are the benefits of PPI infusion in acute GI bleeding?"
      },
      "question2": {
        "correctoption": "Low - 33%",
        "question": "What volume of blood from upper GI source is required to pass melena ?"
      },
      "question3": {
        "correctoption": "It does not improve mucosal visualization.",
        "question": "What is the effect of NG lavage on mucosal visualization at time of endoscopy ?"
      },
      "question4": {
        "correctoption": "7 g/dL",
        "question": "What is hemoglobin transfusion threshold in GI bleeding ?"
      },
      "question5": {
        "correctoption": "Incorporates patient factors (age, SBP, mental status) & labs (INR, albumin) to predict in-hospital mortality",
        "question": "What is the AIMS-65 score?"
      },
      "question6": {
        "correctoption": "Low score (0-1) do not typically require hospital admission or urgent endoscopy.",
        "question": "How is Glasgow-Blatchford score used to perform pre-endoscopy risk stratification?"
      },
      "question7": {
        "correctoption": "SBP, hemoglobin, BUN, pulse, melena, syncome, liver disease, heart failure",
        "question": "What are components of Glasgow-Blatchford score ?"
      },
      "question8": {
        "correctoption": "0.05-0.1cc/min",
        "question": "What rate of bleeding is required for tagged RBC scan ?"
      },
      "question9": {
        "correctoption": "60-70%",
        "question": "What is tagged RBC accuracy of predicting bleeding location ?"
      },
      "question10": {
        "correctoption": "0.3-0.5 cc/min",
        "question": "What rate of bleeding is required for CT angiography ?"
      },
      "question11": {
        "correctoption": "0.5-1.0 cc/min",
        "question": "What rate of bleeding is required for angiography ?"
      },
      "question12": {
        "correctoption": "Reduced need for repeat endoscopy (O.R. 0.55)",
        "question": "What is the main benefit of administering prokinetics prior to endoscopy for GI bleeding ?"
      },
      "question13": {
        "correctoption": "Ablative therapy e.g., APC (or RFA)",
        "question": "What is the most effective treatment for GAVE ?"
      },
      "question14": {
        "correctoption": "Systemic sclerosis, cirrhosis",
        "question": "In what conditions is this condition commonly seen ?"
      },
      "question15": {
        "correctoption": "It is typically not effective.",
        "question": "What is the role of non-selective beta-blockers in treating GAVE ?"
      },
      "question16": {
        "correctoption": "Bleeding from esophageal varices",
        "question": "In what bleeding condition does Sengstaken–Blakemore tube provide effective tamponade ?"
      },
      "question17": {
        "correctoption": "1. Peptic ulcer disease 2. Gastroduodenal erosions 3. Esophagitis",
        "question": "What are the top three causes of UGIB (all-comers) ?"
      },
      "question18": {
        "correctoption": "< 3%",
        "question": "Without treatment, what is rebleeing rate of clean based peptic ulcer ?"
      },
      "question19": {
        "correctoption": "50%",
        "question": "Without treatment, what is rebleeing rate of a peptic ulcer with a non-bleeding visible vessel ?"
      },
      "question20": {
        "correctoption": "Up to 35%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with an adherent clot ?"
      },
      "question21": {
        "correctoption": "Up to 27%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with oozing?"
      },
      "question22": {
        "correctoption": "< 3%",
        "question": "Without treatment, what is rebleeing rate of clean based peptic ulcer ?"
      },
      "question23": {
        "correctoption": "< 8%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with a flat pigmented spot?"
      },
      "question24": {
        "correctoption": "Up to 35%",
        "question": "Without treatment, what is rebleeding rate of a peptic ulcer with an adherent clot ?"
      },
      "question25": {
        "correctoption": "Up to 35%",
        "question": "In what % of patients with diverticulosis does lower GI bleeding occur ?"
      },
    }
  }
}		
export default class Playquiz extends Component {

  static navigationOptions = {
    header: null
  };
  constructor(props){
    super(props)
    

    //const jdata = jsonData.quiz.quiz1
    //arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
    this.state = {
      question : null,
      options : null,
      correctoption : null,
      countCheck : 0,
      initcount:1,
      totallengh:0,
      Right:1,
      Worng:1,
      OpenView:'FALSE',
      CatValue:'',
      DummyScore:'NO',
      correctQus:null,
      IncorrectQus:null,
    }
 //this.correctQus=[];
    // this.state = {
    //   quizFinish : false,
    //   score: 0
    // }
  }

  componentWillMount() {
     this.qno = 0
    this.score = 0
    this.correctQus=[]
        const CatName = this.props.navigation.getParam('CatNAME');
        console.log('Mapping' +CatName);
         const jdata = jsonData.quiz.quiz1
    arrnew = Object.keys(jdata).map( function(k) { return jdata[k] });
    
        this.setState({
          CatValue:CatName,
          question : arrnew[this.qno].question,
      options : arrnew[this.qno].options,
      correctoption : arrnew[this.qno].correctoption,
      totallengh:arrnew.length,
        })
  }
   Nextview(){
    console.log('kapilklcdcdc'+this.state.OpenView)
    this.setState({
      OpenView:'TRUE',
    })
  }
  _onPressBack(){
    const {goBack} = this.props.navigation
      goBack()
  }
next(itemname){
    this.setState({
      OpenView:'FALSE'
    })
    if(itemname == 'Currect'){
          this.correctQus +=arrnew[this.qno].question;
           this.score += 1
       }else{
      }
    console.log('hhhhhhh')
    if(this.qno < arrnew.length-1){
      this.qno+ 1
console.log('kapil'+this.qno++);
console.log('elese ka calll')
      this.setState({ countCheck: 0,
      initcount:this.state.initcount + 1,
       question: arrnew[this.qno].question, options: arrnew[this.qno].options, correctoption : arrnew[this.qno].correctoption})
    }else{
      this.setState({
        OpenView:'OKAY',
        DummyScore:'YES',
      })
     }
  }


  _quizFinish(score){    
    this.setState({ quizFinish: true, score : score })
  }
  dashboard=()=>{
    this.props.navigation.navigate('Buttomtab')
  }

     renderView = () => {
console.log('kapilkl'+this.state.OpenView)
if(this.state.OpenView =='FALSE' && this.state.DummyScore == 'NO'){
  console.log('Kapil ki jai ho '+this.state.OpenView)
  return(
    <View style={{flex:1,alignItems:'center'}}>
 <View style={{ flexDirection: 'column', justifyContent: "space-between", alignItems: 'center',}}>
  <View style={{width:'99%',alignItems:'center',marginTop:20}}>
  <Text style={{color:'#000',fontSize:26,padding:10,fontWeight:'700'}}>Question {this.state.initcount} of {this.state.totallengh} </Text>
  </View>
  
 <Text style={{color:'#000',fontSize:18,padding:10,fontWeight:'700'}}>{this.state.CatValue}</Text>
  
  <View style={styles.oval} >
        <Text style={styles.welcome}>
          {this.state.question}
        </Text>
     </View>
        <View style={[styles.oval,{marginTop:20, justifyContent:'center',flex:1}]} >
          {/* <Text style={styles.welcome1}>
          {this.state.correctoption}
        </Text> */}
        <View style={{ 
      width:'100%',
        justifyContent: 'center',
        alignItems: 'center',
        }}>
                        <TouchableOpacity
                            style={{ width: '95%',
       height: 42,
        backgroundColor: '#fff',
       justifyContent: 'center',
        borderWidth: 1.8,
          borderColor: '#000',
        borderRadius: 4,
        alignItems: 'center',
       marginTop: 10,
       // marginBottom: 20,
}}
              onPress={() => this.Nextview()}>
         <Text style={{ color: '#000',
         fontSize: 14,
         fontWeight:'700',
        alignSelf: 'center',
     }}>REVEAL ANSWER</Text>
                        </TouchableOpacity>
                  </View>
                     </View> 
      </View> 
      </View>
  )
}else if(this.state.OpenView == 'TRUE' && this.state.DummyScore == 'NO'){
  return(
<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
 <View style={{ flexDirection: 'column', justifyContent: "space-between", alignItems: 'center',}}>
  <View style={{width:'99%',alignItems:'center'}}>
  <Text style={{color:'#000',fontSize:26,padding:10,fontWeight:'700'}}>Answer {this.state.initcount} of {this.state.totallengh} </Text>
  </View>
  {/* <View style={styles.oval} >
        <Text style={styles.welcome}>
          {this.state.question}
        </Text>

     </View> */}
         <View style={[styles.oval,{marginTop:20,}]} >
          <Text style={styles.welcome}>
          {this.state.correctoption}
        </Text>
       </View>
         <View style={[styles.oval,{marginTop:60, justifyContent:'center',flexDirection:'row', flex:3}]} >
<View style={{width:'46%',alignItems:'center'}}>
        <TouchableOpacity onPress={() => this.next('Currect')} >
         <View style={{paddingTop: 5}}>
          <Image  style={{ width:60,height: 60,alignSelf:'center'}}
         source={require('../../../Assets/Images/rights.png')}></Image>
          <Text style={{color:'#000',fontSize:14}}>
          Got it right
        </Text>
          </View>
        </TouchableOpacity >
        </View>
        <View style={{width:'46%',alignItems:'center'}}>
        <TouchableOpacity onPress={() => this.next('Wrong')}>
          <View style={{paddingTop: 5}}>
           <Image  style={{ width:60,height: 60,alignSelf:'center'}}
         source={require('../../../Assets/Images/wrongs.png')}></Image>
          <Text style={{color:'#000',fontSize:14}}>
          Got it wrong
        </Text>
          </View>
        </TouchableOpacity>
        </View>

        </View>
        </View>
        </View>
  )
  
}else if(this.state.OpenView == 'OKAY' && this.state.DummyScore == 'YES'){
  return(
  <View style={styles.innerContainer}>
      <View style={{paddingTop:10}}>
                  <Text style={styles.score}>Great Job</Text>
                  </View>
                  <View style={{height:80, marginTop:10,paddingTop:20}}>
                  <Text style={styles.score}>Congrats you scored {this.core}% </Text>
                  </View>
                  <View style={{alignItems:'center',justifyContent:'center',padding:20}}>
                  <Text style={styles.score1}>The questions you missed are stored in your incorrect answers bank. To access this bank,please click the dashboard button to retrun home and begin another lesson</Text>
                  </View>
                  <View style={{flex:2,padding:10,width:'99%',justifyContent:'center',alignItems:'center'}}>
                    <FlatButton title='VIEW INCORRECT QUESTIONS' />
                     <FlatButton title='DASHBOARD' onPress={this.dashboard} />
                     </View>
                </View>)
}

}
  render() {
    return (
      <View style={{flex:1,backgroundColor:'#fff'}}>
       <StatusBar/>
        {/* <HeaderView/> */}
        <View style={{ marginTop:10,justifyContent: 'center',alignItems: 'center',width:'99%',flexDirection:'row'}}>
        <Image  style={{ width:60,height: 60,alignSelf:'center'}}
         source={require('../../../Assets/Images/logo.png')}></Image>
         <View style={{flexDirection:'column',marginLeft:10}}>
          <Text style={{ color: '#000', fontSize: 11, }}>
              American
           </Text>
           <Text style={{ color: '#000', fontSize: 11,}}>
             Gastroenterological
           </Text>
           <Text style={{ color: '#000', fontSize: 11, }}>
             Association
           </Text>
          </View>
         </View>
        {this.renderView()}
      </View>
    );
  }
}
const scoreCircleSize = 300
const styles = StyleSheet.create({
  score: {
    color: "#000",
    fontSize: 22,
    fontWeight:'700',
        fontStyle: 'italic'
  },
    oval: {
    padding:10,
  width: width * 88/100,
  borderRadius: 20,
  alignItems:'center',
  justifyContent:'center',
  //flex:.5,
 // backgroundColor: 'green'
  },
   score1: {
    color: "#000",
    fontSize: 18,
    alignItems:'center',
    //justifyContent:'center',
    fontWeight:'500',
        fontStyle: 'italic'
  },
  circle: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '99%',
    height: '99%',
    //borderRadius: scoreCircleSize/2,
    //backgroundColor: "green"
  },
  innerContainer: {
    flex: 1,
    width:'99%',
    marginTop:20,
   // justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
  // toolbar:{
  //       backgroundColor:'#81c04d',
  //       paddingTop:30,
  //       paddingBottom:10,
  //       flexDirection:'row'
  //   },
  //   toolbarButton:{
  //       width: 55,
  //       color:'#fff',
  //       textAlign:'center'
  //   },toolbar
  //   toolbarTitle:{
  //       color:'#fff',
  //       textAlign:'center',
  //       fontWeight:'bold',
  //       flex:1
  //   }
});