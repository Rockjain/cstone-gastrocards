import React, { Component } from 'react';
import Quiz  from '../../../Config/QuizPage';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
} from 'react-native';
import HeaderView from '../../../Config/Header'
import StatusBar from '../../../Assets/StatusBar';
import Icon from 'react-native-vector-icons/FontAwesome';
import FlatButton from '../../../Config/Button/Button.js';
import firebase from '../../../Database/firebase';
export default class Questios extends Component {

  static navigationOptions = {
    header: null
  };
  constructor(props){
    super(props)
    this.state = {
      quizFinish : false,
      score: 0,
      catName:props.navigation.getParam('Name')
    }
  }


  componentWillMount(){
      
    firebase.database().ref('quiz/').on('value', function (snapshot) {
       //jsonData= snapshot.val();
          console.log('rohit kain'+JSON.stringify(snapshot.val()))
         //const jsonData1=(snapshot.val())
         //jsonData12=JSON.stringify(jsonData1)
        
    
    });
  }
  _onPressBack(){
    const {goBack} = this.props.navigation
      goBack()
  }
  _quizFinish(score){    
    this.setState({ quizFinish: true, score : score })
  }
  dashboard=()=>{
    this.props.navigation.navigate('Buttomtab')
  }
  _scoreMessage(score){
      return (<View style={styles.innerContainer}>
      <View style={{paddingTop:10}}>
                  <Text style={styles.score}>Great Job Rohit !</Text>
                  </View>
                  <View style={{height:80, marginTop:10,paddingTop:20}}>
                  <Text style={styles.score}>Congrats you scored {score}% </Text>
                  </View>
                  <View style={{alignItems:'center',justifyContent:'center',padding:20}}>
                  <Text style={styles.score1}>The questions you missed are stored in your incorrect answers bank. To access this bank,please click the dashboard button to retrun home and begin another lesson</Text>
                  </View>
                  <View style={{flex:2,padding:10,width:'99%',justifyContent:'center',alignItems:'center'}}>
                    <FlatButton title='VIEW INCORRECT QUESTIONS' />
                     <FlatButton title='DASHBOARD' onPress={this.dashboard} />
                     </View>
                </View>)
    }
  render() {
    return (
      <View style={{flex:1,backgroundColor:'#fff'}}>
       <StatusBar/>
        {/* <HeaderView/> */}
        <View style={{ marginTop:10,justifyContent: 'center',alignItems: 'center',width:'99%',flexDirection:'row'}}>
        <Image  style={{ width:60,height: 60,alignSelf:'center'}}
         source={require('../../../Assets/Images/logo.png')}></Image>
         <View style={{flexDirection:'column',marginLeft:10}}>
          <Text style={{ color: '#000', fontSize: 11, }}>
              American
           </Text>
           <Text style={{ color: '#000', fontSize: 11,}}>
             Gastroenterological
           </Text>
           <Text style={{ color: '#000', fontSize: 11, }}>
             Association
           </Text>
          </View>
         </View>
       { this.state.quizFinish ? <View style={styles.container}>
           <View style={styles.circle}>

             { this._scoreMessage(this.state.score) }
           </View>

       </View> :  <Quiz quizFinish={(score) => this._quizFinish(score)} /> }

            
        
      </View>
    );
  }
}
const scoreCircleSize = 300
const styles = StyleSheet.create({
  score: {
    color: "#000",
    fontSize: 22,
    fontWeight:'700',
        fontStyle: 'italic'
  },
   score1: {
    color: "#000",
    fontSize: 18,
    alignItems:'center',
    //justifyContent:'center',
    fontWeight:'500',
        fontStyle: 'italic'
  },
  circle: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '99%',
    height: '99%',
    //borderRadius: scoreCircleSize/2,
    //backgroundColor: "green"
  },
  innerContainer: {
    flex: 1,
    width:'99%',
    marginTop:20,
   // justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F5FCFF',
  },
});