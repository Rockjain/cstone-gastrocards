import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

export default function CatTab({title, onPress}) {
  return (
   <View style={{ height: 86,
                        width:'47%',
      borderColor: '#000',
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
      borderWidth: 1.5}}>
       <TouchableOpacity
             onPress={() => this.props.navigation.navigate('PlayQuizStack')}>
                         <Text style={{fontSize:14,color:'#000',fontWeight: '700'}}>{title}</Text>
                       </TouchableOpacity>
                        </View>  
  );
}

const styles = StyleSheet.create({
  parentButtonStyle: {
    flex: 1,
    margin: 5,
  },
  buttonStyle: {
    borderRadius: 16,
    backgroundColor: '#03a1fc',
    padding: 10,
  },
  textColor: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
